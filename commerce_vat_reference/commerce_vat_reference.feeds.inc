<?php

/**
 * @file
 * Commerce VAT reference field feeds mapper.
 */

/**
 * Implements hook_feeds_processor_targets_alter().
 *
 * @see FeedsProcessor::getMappingTargets()
 */
function commerce_vat_reference_feeds_processor_targets_alter(&$targets, $entity_type, $bundle_name) {
  foreach (field_info_instances($entity_type, $bundle_name) as $name => $instance) {
    $info = field_info_field($name);

    if ($info['type'] === 'commerce_vat_rate_reference') {
      $targets[$name] = array(
        'name' => check_plain($instance['label']),
        'callback' => 'commerce_vat_reference_feeds_set_target',
        'description' => t('The @label field of the entity.', array('@label' => $instance['label'])),
      );
    }
  }
}

/**
 * Callback for mapping var reference fields.
 */
function commerce_vat_reference_feeds_set_target($source, $entity, $target, $value) {
  if (!isset($value)) {
    return;
  }

  if (!is_array($value)) {
    $value = array($value);
  }

  $info = field_info_field($target);

  // Iterate over all values.
  $field = array(LANGUAGE_NONE => array());

  foreach ($value as $i => $v) {

    if ($info['cardinality'] == $i) {
      break;
    }

    if (is_object($v) && ($v instanceof FeedsElement)) {
      $v = $v->getValue();
    }

    if (is_scalar($v)) {
      $field[LANGUAGE_NONE][$i]['value'] = $v;
    }
  }

  $entity->$target = $field;
}
